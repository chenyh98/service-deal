package org.jlu.dede.serviceDeal.service;

import com.codingapi.txlcn.tc.annotation.DTXPropagation;
import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import org.jlu.dede.serviceDeal.feign.*;
import org.jlu.dede.publicUtlis.model.*;
import org.jlu.dede.serviceDeal.JsonException.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
@Service
public class ServiceImpl implements ServiceInt {
    @Autowired
    private DriverDateAccess driverRepository;
    @Autowired
    AccountDataAccess accountRepository;
    @Autowired
    OrderDateAccess orderRepository;
    @Autowired
    RecordDateAccess recordRepository;
    @Autowired
    PassengerDateAccess passengerRepository;


    @Override
    @LcnTransaction(propagation = DTXPropagation.REQUIRED)
    @Transactional(rollbackFor = Exception.class)
    public boolean payOrder(Order order) {
            //得到司机乘客对象和最终价格
            Integer state=order.getState();
            if(state.equals(4))
            {
                throw new repayException("重复支付！");
            }

 //<--*****modify*******-->

            Passenger passenger=accountRepository.findPassengerByOrder(order.getId());
            Driver driver = accountRepository.findDriverByOrder(order.getId());
            Order order1=orderRepository.getByID(order.getId());
            //Passenger passenger = order.getPassenger();//根据乘客id找到乘客
           // Driver driver = order.getDriver();

            BigDecimal finalprice = order.getFinalMoney();
            if (passenger == null )
            {
                throw  new noPassengerException("支付时乘客为null");
            }
            if (driver == null )
            {
                throw  new noDriverException("支付时乘客为null");
            }
            if(finalprice==null)
                finalprice=new BigDecimal(0);

//<--*****modify*******-->
            //获取乘客司机账户
            Account accountP=accountRepository.getByPassenger(passenger.getId());
            Account accountD=accountRepository.getByDriver(driver.getId());

    //test!!
            //修改金钱
           if(accountD==null)
           {
              throw new noAccountException("支付时司机账户为空！");
           }
            if(accountP==null)
            {
                throw new noAccountException("支付时乘客账户为空！");
            }

            BigDecimal Pcurrentmoney = accountP.getMoney();
            BigDecimal Dcurrentmoney = accountD.getMoney();
            if(Dcurrentmoney==null)
                Dcurrentmoney=new BigDecimal(0);

            if(Pcurrentmoney.compareTo(finalprice)==-1)
            {
                throw new moneyNotEnough("乘客余额不足");
            }

            accountD.setMoney(Dcurrentmoney.add(finalprice));
            accountP.setMoney(Pcurrentmoney.subtract(finalprice));
            accountRepository.updateAccount(accountD.getId(),accountD);
            accountRepository.updateAccount(accountP.getId(),accountP);

            //修改订单状态
            order.setState(4);
            order.setPayTime(getdate());
            order.setType(order1.getType());
            orderRepository.update(order,order.getId());

            //修改apassenger 的当前订单
              passenger.setOrder(null);
              passenger.setAccount(accountP);
            passengerRepository.updateCurrentOrder(passenger);

            //交易记录增加
            Record record = new Record();
            record.setAccepter(accountD);
            record.setPayer(accountP);
            record.setMoney(finalprice);
            record.setType(3);
            record.setTradeTime(getdate());
            try
            {
                recordRepository.addRecord(record);
            } catch (Exception e)
            { throw new addRecordFailed("增加账户失败"); }

           return true;
        }



    @Override
    @LcnTransaction(propagation = DTXPropagation.REQUIRED)
    @Transactional(rollbackFor = Exception.class)
    public boolean recharge(Integer id, BigDecimal money)
    {
            Passenger passenger = passengerRepository.getByID(id);
           if (passenger == null)
            {
                throw  new noPassengerException("充值时乘客为null");
            }
//<--*****modify*******-->

            Account Paccount = accountRepository.getByPassenger(id); //获取乘客账户

            if(Paccount==null)
            {
                throw new noAccountException("充值时乘客账户为空");
            }
            if(money.compareTo(BigDecimal.ZERO)!=1)
            {
                throw new moneyNotPositive("充值金额为非正数！");
            }
            else
            {
                BigDecimal currentmoney = Paccount.getMoney();
                if(currentmoney==null)
                    currentmoney=new BigDecimal(0);
                Paccount.setMoney(currentmoney.add( money));
                accountRepository.updateAccount(Paccount.getId(),Paccount);
                //创建订单，充值没有支付方
                Record record = new Record();
                record.setAccepter(Paccount);
                record.setPayer(null);
                record.setMoney(money);
                record.setType(2);
                record.setTradeTime(getdate());
                try
                {
                    recordRepository.addRecord(record);
                } catch (Exception e)
                { throw new addRecordFailed("增加账户失败"); }
                return true;
            }

    }

   @Override
   @LcnTransaction(propagation = DTXPropagation.REQUIRED)
   @Transactional(rollbackFor = Exception.class)
    public boolean withdraw(Integer id, BigDecimal money) {
           if(money.compareTo(BigDecimal.ZERO)!=1)
           {
               throw  new moneyNotPositive("提现金额不为正数");
           }

            Driver driver = driverRepository.getById(id);
            if (driver == null)
            {
                throw new noDriverException("提现司机为NULL");
            }
//<--*****modify*******-->
           Account Daccount = accountRepository.getByDriver(driver.getId()); //获取司机账户
           if(Daccount==null)
           {
               throw new noAccountException("司机账户为空！");
           }

                Integer accountId = Daccount.getId();
                BigDecimal currentmoney = Daccount.getMoney();
                if(currentmoney==null)
                    currentmoney=new BigDecimal(0);
                if (currentmoney.compareTo(money) == -1) {
                    throw new moneyNotEnough("提现金额不足！");
                }
                else
                {
                    Daccount.setMoney(currentmoney.subtract(money));
                    accountRepository.updateAccount(accountId, Daccount);
                    //创建交易记录,提现没有接受方
                    Record record = new Record();
                    record.setPayer(Daccount);
                    record.setAccepter(null);
                    record.setMoney(money);
                    record.setType(1);
                    record.setTradeTime(getdate());
                    try
                    {
                        recordRepository.addRecord(record);
                    } catch (Exception e)
                    { throw new addRecordFailed("增加账户失败"); }
                    //recordRepository.addRecord(record);

                   return true;
                }
    }

    @Override
    public List<Record> findByPayer(Integer id) {
        Passenger passenger=passengerRepository.getByID(id);
        if(passenger==null)
            return null;
        //得到乘客对应的账户
       // Account account=passenger.getAccount();
        //<--*****modify*******-->
        Account account = accountRepository.getByPassenger(passenger.getId());
        if(account==null)
            throw new noAccountException("乘客账户为空异常");
        List<Record> list=recordRepository.findByPayerLike(account.getId());
        if(list==null)
            return null;
        else
            return list;

    }

    @Override
    public Record findByRecordId(Integer id) {
        //交易记录的@Autowired
        Record record=recordRepository.getById(id);
        if(record!=null)
            return record;
        return null;
    }

   static String getdate()
    {
        Date d =new Date();
        SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(d);
    }
}
