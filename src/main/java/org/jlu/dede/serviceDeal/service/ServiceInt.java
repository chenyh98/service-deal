package org.jlu.dede.serviceDeal.service;


import org.jlu.dede.publicUtlis.model.Order;
import org.jlu.dede.publicUtlis.model.Record;

import java.math.BigDecimal;
import java.util.List;

public interface ServiceInt {
    //交易付钱（乘客账户减钱，乘客状态改变，订单状态改变，交易记录增加，司机账户加钱）
   boolean payOrder(Order order);

    //充值（乘客账户加钱，交易记录增加）
    boolean recharge(Integer id, BigDecimal money);

    //提现（司机账户减钱，交易记录增加）
    boolean withdraw(Integer id, BigDecimal money);
    //根据支付方乘客Id查询
    List<Record> findByPayer(Integer id);
    //根据记录id查
    Record findByRecordId(Integer id);
}
