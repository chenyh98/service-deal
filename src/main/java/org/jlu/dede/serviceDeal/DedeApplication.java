package org.jlu.dede.serviceDeal;


//import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients(basePackages = {"org.jlu.dede.serviceDeal.feign"})
//@EnableDistributedTransaction
public class DedeApplication {

    public static void main(String[] args) {
        SpringApplication.run(DedeApplication.class, args);
    }

}
