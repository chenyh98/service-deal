package org.jlu.dede.serviceDeal.JsonException;

import org.jlu.dede.publicUtlis.json.RestException;

public class addRecordFailed extends RestException {
    public addRecordFailed(String message)
    {
        super(735,message);
        setErrorCode(Integer.valueOf(735));
    }
}
