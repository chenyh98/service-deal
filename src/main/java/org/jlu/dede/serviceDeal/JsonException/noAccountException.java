package org.jlu.dede.serviceDeal.JsonException;

import org.jlu.dede.publicUtlis.json.RestException;

public class noAccountException extends RestException {
    public noAccountException(String message)
    {
        super(730,message);
        setErrorCode(Integer.valueOf(730));
    }
}
