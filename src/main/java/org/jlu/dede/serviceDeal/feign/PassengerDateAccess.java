package org.jlu.dede.serviceDeal.feign;

import org.jlu.dede.publicUtlis.model.Passenger;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@FeignClient(name = "data-access",url = "10.100.0.2:8765")  //@ FeignClient（“服务名”），来指定调用哪个服务
public interface PassengerDateAccess {
    @RequestMapping (value = "/passengers/{id}",method = RequestMethod.GET)
    Passenger getByID(@PathVariable("id") Integer id);

   @RequestMapping(value="/passengers",method = RequestMethod.POST)
   Passenger updateCurrentOrder(@RequestBody Passenger passenger);
}
