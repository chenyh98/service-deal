package org.jlu.dede.serviceDeal.feign;

import org.jlu.dede.publicUtlis.model.Order;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "data-access",url = "10.100.0.2:8765")
public interface OrderDateAccess {
    @RequestMapping(value = "/orders",method = RequestMethod.POST)
    void addOrder(@RequestBody Order order);
    @RequestMapping(value = "/orders/{id}",method = RequestMethod.POST)
    void update(@RequestBody Order order,@PathVariable("id") Integer id);

    @RequestMapping(value = "/orders/{id}", method = RequestMethod.GET)
    Order getByID(@PathVariable("id") Integer id);
}
