package org.jlu.dede.serviceDeal.feign;

import org.jlu.dede.publicUtlis.model.Account;
import org.jlu.dede.publicUtlis.model.Record;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "data-access",url = "10.100.0.2:8765")  //@ FeignClient（“服务名”），来指定调用哪个服务
public interface RecordDateAccess {
    @RequestMapping(value = "/records/payer/{id}",method = RequestMethod.GET)
    List<Record> findByPayerLike(@PathVariable("id") Integer id);

    @RequestMapping(value = "/records",method = RequestMethod.POST)
    void addRecord(@RequestBody Record record);

    @RequestMapping(value = "/records/{id}",method = RequestMethod.GET)
    Record getById(@PathVariable("id") Integer id);
}
