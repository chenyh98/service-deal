package org.jlu.dede.serviceDeal.feign;

import org.jlu.dede.publicUtlis.model.Driver;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@FeignClient(name = "data-access",url = "10.1000.0.2:8765")
public interface DriverDateAccess {
    @RequestMapping (value="/drivers/{id}",method = RequestMethod.GET)
     Driver getById(@PathVariable("id") Integer id);
}
