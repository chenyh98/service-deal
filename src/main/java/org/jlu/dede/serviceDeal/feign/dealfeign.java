package org.jlu.dede.serviceDeal.feign;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Response;
import feign.Util;
import feign.codec.ErrorDecoder;
import org.jlu.dede.publicUtlis.json.RestError;
import org.jlu.dede.publicUtlis.json.RestException;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
@Configuration
public class dealfeign implements ErrorDecoder {
    @Override
//    @HystrixCommand(ignoreExceptions = {RestException.class})
    public RestException decode(String s, Response response) {
        if (response.status() / 100 == 2) return null;
        try {
            String body = Util.toString(response.body().asReader());
            ObjectMapper om = new ObjectMapper();
            RestError re =  om.readValue(body.getBytes("UTF-8"), RestError.class);
            RestException exception = new RestException(re.getErrorCode(), re.getMessage());
            return exception;
        } catch (IOException e) {
            return new RestException(e);
        }
    }
}
