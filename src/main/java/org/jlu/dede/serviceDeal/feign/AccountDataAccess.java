package org.jlu.dede.serviceDeal.feign;

import org.jlu.dede.publicUtlis.model.Account;
import org.jlu.dede.publicUtlis.model.Driver;
import org.jlu.dede.publicUtlis.model.Passenger;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.web.bind.annotation.*;


@FeignClient(name = "data-access",url = "10.100.0.2:8765")  //@ FeignClient（“服务名”），来指定调用哪个服务
public interface AccountDataAccess {
    @RequestMapping(value="/accounts",method = RequestMethod.POST)
    void addAccount(@RequestBody Account account);
    @RequestMapping(value = "/accounts/{id}",method = RequestMethod.POST)
    void updateAccount(@PathVariable("id") Integer id, @RequestBody Account account);

    @RequestMapping(value =" accounts/{id}",method = RequestMethod.GET)
     Account getByID(@PathVariable("id") Integer id);

    @RequestMapping(value ="/passengers/order/{id}",method = RequestMethod.GET)
     Passenger findPassengerByOrder(@PathVariable("id") Integer id);

    @RequestMapping(value ="/drivers/order/{id}",method = RequestMethod.GET)
     Driver findDriverByOrder(@PathVariable("id") Integer id);

    @RequestMapping(value ="/accounts/driver/{id}",method = RequestMethod.GET)
    Account getByDriver(@PathVariable("id") Integer id);

    @RequestMapping(value ="/accounts/passenger/{id}",method = RequestMethod.GET)
    Account getByPassenger(@PathVariable("id") Integer id);
}