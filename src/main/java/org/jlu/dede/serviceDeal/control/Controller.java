package org.jlu.dede.serviceDeal.control;

import org.jlu.dede.publicUtlis.model.Order;
import org.jlu.dede.publicUtlis.model.Record;
import org.jlu.dede.serviceDeal.service.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

//交易类型1提现 2充值 3正常交易
@RestController
public class Controller {
    @Autowired
    ServiceImpl serviceimpl;

    //交易付钱
    @PostMapping("/passenger/deal/payOrder")
    public boolean payOrder(@RequestBody Order order)
    {
     return  serviceimpl.payOrder(order);
    }

    //乘客充值
    @PostMapping("/passenger/deal/recharge")
    public boolean recharge(@RequestParam Integer id,@RequestParam BigDecimal money)
    {
        return serviceimpl.recharge(id,money);
    }

    //司机提现
   @PostMapping("/driver/deal/withdraw")
    public boolean withdraw(@RequestParam Integer id,@RequestParam BigDecimal money)
   {
       return serviceimpl.withdraw(id,money);
   }
    //通过支付方（乘客）id查询交易记录
   @GetMapping("/passenger/deal/query")
    public List<Record> findByPayer(@RequestParam Integer id)
   {
       return serviceimpl.findByPayer(id);
   }

    //通过交易记录id查询交易记录
    @RequestMapping(value="passenger/record/deal/query",method = RequestMethod.GET)
    public Record findByRecord(Integer id)
    {
        return serviceimpl.findByRecordId(id);
    }


}
